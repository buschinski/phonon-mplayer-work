/*

Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 or 3 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

/*****************************************************************************
 * libVLC backend for the Phonon library                                     *
 *                                                                           *
 * Copyright (C) 2007-2008 Tanguy Krotoff <tkrotoff@gmail.com>               *
 * Copyright (C) 2008 Lukas Durfina <lukas.durfina@gmail.com>                *
 * Copyright (C) 2009 Fathi Boudra <fabo@kde.org>                            *
 * Copyright (C) 2009-2010 vlc-phonon AUTHORS                                *
 *                                                                           *
 * This program is free software; you can redistribute it and/or             *
 * modify it under the terms of the GNU Lesser General Public                *
 * License as published by the Free Software Foundation; either              *
 * version 2.1 of the License, or (at your option) any later version.        *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 * Lesser General Public License for more details.                           *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public          *
 * License along with this package; if not, write to the Free Software       *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA *
 *****************************************************************************/

#include "StreamReader.h"
#include "MediaObject.h"

#include <phonon/streaminterface.h>

#include "TemporaryFifo.h"
#include "libmplayer/LibMPlayerLogger.h"

QT_BEGIN_NAMESPACE
#ifndef QT_NO_PHONON_ABSTRACTMEDIASTREAM
namespace Phonon
{
namespace MPlayer
{

StreamReader::StreamReader(const Phonon::MediaSource &source, MediaObject *parent)
    : m_size(0)
    , m_seekable(false)
    , m_mediaObject(parent)
{
    m_fifo = new TemporaryFifo("phonon-mplayer");
    if (!m_fifo->createfifo())
    {
        LibMPlayerCritical() << "mkfifo failed, what now?";
    }

    connectToSource(source);
}

StreamReader::~StreamReader()
{
    m_fifo->close();
    delete m_fifo;
}

void StreamReader::writeData(const QByteArray& data)
{
    QMutexLocker lock(&m_mutex);

    if (!m_fifo->isOpen())
    {
        if (!m_fifo->open())
        {
            LibMPlayerCritical() << "open fifo failed, what now?";
        }
    }

    m_fifo->write(data);

    m_waitingForData.wakeAll();

    if (m_mediaObject->state() != Phonon::BufferingState
        && m_mediaObject->state() != Phonon::LoadingState)
        enoughData();
}

QString StreamReader::fifoName() const
{
    return m_fifo->filePath();
}

void StreamReader::endOfData()
{
    m_waitingForData.wakeAll();
}

void StreamReader::setStreamSize(qint64 newSize)
{
    m_size = newSize;
}

void StreamReader::setStreamSeekable(bool s)
{
    m_seekable = s;
}

bool StreamReader::streamSeekable() const
{
    return m_seekable;
}

#include "StreamReader.moc"

}// namespace MPlayer
}// namespace Phonon
#endif //QT_NO_PHONON_ABSTRACTMEDIASTREAM

QT_END_NAMESPACE
