/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Bernd Buschinski <b.buschinski@googlemail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef TEMPORARYFIFO_H
#define TEMPORARYFIFO_H

#include <QString>
#include <QThread>
#include <QMutex>

class QFile;

class TemporaryFifo : public QThread
{
    Q_OBJECT
public:
    explicit TemporaryFifo(const QString& templateName, QObject* parent = 0);
    
    ~TemporaryFifo();
    
    bool createfifo();
    
    bool open();
    bool close();
    
    bool isOpen();

    QString fileName() const;
    QString filePath() const;
    
    bool write(const QByteArray &data);

protected:
    virtual void run();
    
private:
    QByteArray m_buffer;
    QString m_templateName;
    QString m_fileName;
    QFile* m_file;
    QMutex m_mutex;
};

#endif // TEMPORARYFIFO_H
