/*  This file is part of the KDE project.

Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 or 3 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

/*****************************************************************************
 * libVLC backend for the Phonon library                                     *
 *                                                                           *
 * Copyright (C) 2007-2008 Tanguy Krotoff <tkrotoff@gmail.com>               *
 * Copyright (C) 2008 Lukas Durfina <lukas.durfina@gmail.com>                *
 * Copyright (C) 2009 Fathi Boudra <fabo@kde.org>                            *
 * Copyright (C) 2009-2010 vlc-phonon AUTHORS                                *
 *                                                                           *
 * This program is free software; you can redistribute it and/or             *
 * modify it under the terms of the GNU Lesser General Public                *
 * License as published by the Free Software Foundation; either              *
 * version 2.1 of the License, or (at your option) any later version.        *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 * Lesser General Public License for more details.                           *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public          *
 * License along with this package; if not, write to the Free Software       *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA *
 *****************************************************************************/

#ifndef PHONON_STREAMREADER_H
#define PHONON_STREAMREADER_H

#include <phonon/mediasource.h>
#include <phonon/streaminterface.h>

#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>

class TemporaryFifo;

QT_BEGIN_NAMESPACE

#ifndef QT_NO_PHONON_ABSTRACTMEDIASTREAM

namespace Phonon
{
class MediaSource;
namespace MPlayer
{

class MediaObject;


class StreamReader : public Phonon::StreamInterface
{
    Q_INTERFACES(Phonon::StreamInterface)
public:
    StreamReader(const Phonon::MediaSource &source, MediaObject *parent);
    virtual ~StreamReader();

    virtual void setStreamSeekable(bool s);
    virtual void setStreamSize(qint64 newSize);
    virtual void endOfData();
    virtual void writeData(const QByteArray& data);
    
    QString fifoName() const;
    bool streamSeekable() const;

protected:
    quint64 m_size;
    bool m_seekable;
    QMutex m_mutex;
    QWaitCondition m_waitingForData;
    MediaObject *m_mediaObject;
    
    TemporaryFifo* m_fifo;
};

}
}

#endif //QT_NO_PHONON_ABSTRACTMEDIASTREAM

QT_END_NAMESPACE

#endif // MPLAYER_STREAMREADER_H
