/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Bernd Buschinski <b.buschinski@googlemail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "TemporaryFifo.h"

#include <ctime>
#include <cstdlib>

#include <QFile>
#include <QDir>
#include <QDebug>

#ifndef Q_OS_WIN32
#   include <sys/stat.h>
#   include <sys/types.h>
#else
    //names pipe includes
#endif

#include "libmplayer/LibMPlayerLogger.h"


TemporaryFifo::TemporaryFifo(const QString& templateName, QObject* parent)
    : QThread(parent),
      m_templateName(templateName),
      m_file(0)
{

}

TemporaryFifo::~TemporaryFifo()
{
    close();
}

bool TemporaryFifo::createfifo()
{
    if (m_file && isOpen())
    {
        LibMPlayerCritical() << "fifo is already open";
        return false;
    }
    
    qsrand(time(NULL));
    
    QString name = m_templateName;
    for (int i = 0; i < 6; ++i)
    {
        name += (char)(uint( (double(25) * (qrand() / (RAND_MAX + 1.0f))) + 0.5f ) + 65);
    }
    
    int count = 0;
    static const int maxLength = 200;
    while (QFile::exists(QDir::tempPath() + QDir::separator() + name) && count < maxLength)
    {
        name += (char)(uint( (double(25) * (qrand() / (RAND_MAX + 1.0f))) + 0.5f ) + 65);
        count++;
    }
    
    if (count >= maxLength)
    {
        LibMPlayerCritical() << "can not find alternative filename";
        return false;
    }
    
    QString filePath = QDir::tempPath() + QDir::separator() + name;
    
#ifndef Q_OS_WIN32
    qDebug() << "mkfifo now: " << filePath;
    int status = ::mkfifo(filePath.toAscii().constData(),
                 S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH);
    if (status != 0)
    {
        LibMPlayerCritical() << "unabled to create fifo " << filePath;
        return false;
    }
    
#else
    //TODO implement me with named pipes
#endif
    m_fileName = name;
    return true;
}

bool TemporaryFifo::open()
{
    if (m_fileName.isEmpty() || m_file)
    {
        LibMPlayerCritical() << "no filename given" << m_fileName;
        return false;
    }

    qDebug() << "open fifo now: " << filePath();
    m_file = new QFile(filePath(), this);
    if (!m_file->open(QIODevice::WriteOnly /*| QIODevice::Truncate*/))
    {
        LibMPlayerCritical() << "can not open fifo";
        delete m_file;
        m_file = 0;
        return false;
    }

    return true;
}

bool TemporaryFifo::close()
{
    if (m_file)
    {
        m_file->isOpen();
        m_file->close();
        delete m_file;
        m_file = 0;

#ifndef Q_OS_WIN32
        unlink(QString(QDir::tempPath() + QDir::separator() + m_fileName).toAscii().constData());
#else
        //close named pipe
#endif
    }
    return true;
}

bool TemporaryFifo::isOpen()
{
    if (!m_file)
        return false;
    return m_file->isOpen();
}

QString TemporaryFifo::fileName() const
{
    return m_fileName;
}

QString TemporaryFifo::filePath() const
{
    return QDir::tempPath() + QDir::separator() + m_fileName;
}

bool TemporaryFifo::write(const QByteArray& data)
{
    if (!(m_file && isOpen()))
    {
        LibMPlayerCritical() << "file not open, can not write";
        return false;
    }

    m_mutex.lock();
    m_buffer.append(data);
    m_mutex.unlock();
    
    if (!isRunning())
    {
        start(QThread::InheritPriority);
    }
    return true;
}

void TemporaryFifo::run()
{
    m_mutex.lock();
    while (!m_buffer.isEmpty())
    {
        QByteArray buffer = m_buffer;
        m_buffer.clear();
        m_mutex.unlock();
        
        LibMPlayerDebug() << "write start!" << buffer.size() << m_buffer.size();
        m_file->write(buffer);
        
        m_mutex.lock();
    }
    m_mutex.unlock();
}
